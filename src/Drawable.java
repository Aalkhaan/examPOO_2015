
public interface Drawable {
	void draw(SimpleDrawer simpleDrawer);
}

