public class Point extends Geometry {
    /**
     * x coordinate
     */
    private final int x;

    /**
     * y coordinate
     */
    private final int y;

    public Point(int x, int y, String name) {
        super(name);
        this.x = x;
        this.y = y;
        setBarycenter(this);
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
        setBarycenter(this);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "un Point " + getName() + " : (" + x + ", " + y + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Point)) return false;
        return ((Point) o).getX() == x && ((Point) o).getY() == y;
    }

    @Override
    public int hashCode() {
        return x + y;
    }
}
