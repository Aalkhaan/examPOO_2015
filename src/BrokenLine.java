import javax.management.OperationsException;
import java.util.HashSet;
import java.util.LinkedList;

public class BrokenLine extends Geometry {
    /**
     * Utilisation d'une LinkedList car on a besoin d'un ordre et d'insérer/retirer
     * des éléments aux extremités en temps constant.
     */
    private final LinkedList<Point> points;

    private final HashSet<Point> pointsSet;

    private int XCoordinatesSum = 0;
    private int YCoordinatesSum = 0;

    public BrokenLine(Point point1, Point point2, String name) {
        super(name);
        points = new LinkedList<>();
        pointsSet = new HashSet<>();
        add(point1);
        add(point2);
    }

    public BrokenLine(Point point1, Point point2) {
        points = new LinkedList<>();
        pointsSet = new HashSet<>();
        points.add(point1);
        points.add(point2);
        pointsSet.add(point1);
        pointsSet.add(point2);
        setBarycenter(new Point(    (point1.getX() + point2.getX()) / 2,
                (point1.getY() + point2.getY()) / 2,
                "Barycenter"));
    }

    /**
     * Add a Point in O(1).
     */
    public void add(Point point) {
        points.add(point);

        // Avant d'ajouter le point au set, on vérifie s'il y figurait déjà afin de ne pas le prendre en compte
        // dans le calcul du barycentre le cas échéant.
        if (pointsSet.add(point)) {
            XCoordinatesSum += point.getX();
            YCoordinatesSum += point.getY();
            setBarycenter(new Point(XCoordinatesSum / pointsSet.size(), YCoordinatesSum / pointsSet.size()));
        }
    }

    /**
     * Remove the first Point of {@code points} in O(1)
     * @throws OperationsException points' size must stay over 2
     */
    public void removeFirst() throws OperationsException {
        if (points.size() <= 2) {
            throw new OperationsException("Impossible to remove more points to this BrokenLine, " +
                    "it only has 2 points.");
        }
        pointsSet.remove(points.remove());
    }

    @Override
    public String toString() {
        return "une BrokenLine " + getName() + " de barycentre " + getBarycenter();
    }

    public LinkedList<Point> getPoints() {
        return points;
    }
}
