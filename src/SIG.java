import java.util.*;

public class SIG implements Drawable {
    /**
     * Permet de préserver l'ordre des entités et d'insérer des éléments en O(1)
     */
    private final LinkedList<Geometry> entities = new LinkedList<>();

    /**
     * Permet de retrouver l'entité associée à un nom en O(1)
     */
    private final Map<String, Geometry> names = new HashMap<>();

    public void add(Geometry geom) {
        if (names.containsKey(geom.getName())) {
            throw new IllegalArgumentException("Geometry entity with this name already exists");
        }
        entities.add(geom);
        names.put(geom.getName(), geom);
    }

    public Geometry getGeometry(String name) {
        return names.get(name);
    }

    public Iterator<String> nameIterator() {
        return names.keySet().iterator();
    }

    public Iterator<Geometry> geometryIterator() {
        return entities.iterator();
    }

    @Override
    public String toString() {
        return entities.toString();
    }

    @Override
    public void draw(SimpleDrawer simpleDrawer) {
        for (Geometry geometry : entities) {
            if (geometry instanceof Point) {
                simpleDrawer.drawPoint(((Point) geometry).getX(), ((Point) geometry).getY());
            }
            else if (geometry instanceof BrokenLine) {
                Object[] points = ((BrokenLine) geometry).getPoints().toArray();
                for (int i = 0; i < points.length - 1; i++) {
                    simpleDrawer.drawLineSegment(((Point) points[i]).getX(), ((Point) points[i]).getY(),
                            ((Point) points[i+1]).getX(), ((Point) points[i+1]).getY());
                }
            }
            else if (geometry instanceof Complex) {
                LinkedList<BrokenLine> brokenLines = ((Complex) geometry).getBrokenLines();
                for (BrokenLine brokenLine : brokenLines) {
                    Object[] points = brokenLine.getPoints().toArray();
                    for (int i = 0; i < points.length - 1; i++) {
                        simpleDrawer.drawLineSegment(((Point) points[i]).getX(), ((Point) points[i]).getY(),
                                ((Point) points[i+1]).getX(), ((Point) points[i+1]).getY());
                    }
                }

                LinkedList<Point> points = ((Complex) geometry).getPoints();
                for (Point point : points) {
                    simpleDrawer.drawPoint(point.getX(), point.getY());
                }

            }
        }
    }
}
