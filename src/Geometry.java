public abstract class Geometry {
    private final String name;
    private Point barycenter;
    private static int automaticDigit;

    public Geometry(String name) {
        this.name = name;
    }

    public Geometry() {
        name = "Geometry" + automaticDigit;
        automaticDigit++;
    }

    public String getName() {
        return name;
    }

    public Point getBarycenter() {
        return barycenter;
    }

    protected void setBarycenter(Point barycenter) {
        this.barycenter = barycenter;
    }



    @Override
    public abstract String toString();
}
