import java.util.LinkedList;

public class Complex extends Geometry {
    private final LinkedList<BrokenLine> brokenLines = new LinkedList<>();
    private final LinkedList<Point> points = new LinkedList<>();

    public void addBrokenLine(BrokenLine brokenLine) {
        brokenLines.add(brokenLine);
    }

    public void addPoint(Point point) {
        points.add(point);
    }

    @Override
    public String toString() {
        return null;
    }

    public LinkedList<BrokenLine> getBrokenLines() {
        return brokenLines;
    }

    public LinkedList<Point> getPoints() {
        return points;
    }
}
